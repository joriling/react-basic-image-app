import { createAsyncThunk } from "../store/helpers"
import { parseAxiosResponse } from "../utils/helper"
import Http from "../utils/Http"

export const getPicsumPhotos = createAsyncThunk('GET_PICSUM_PHOTOS', async () => {
  return parseAxiosResponse(Http.get(`v2/list`))
})

