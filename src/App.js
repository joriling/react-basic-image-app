import React from 'react'
import styles from './styles/main.scss'
import { Photos } from './components'

function App() {
  return (
    <Photos/>
  )
}

export default App