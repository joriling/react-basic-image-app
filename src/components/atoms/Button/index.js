import React from 'react'
import PropTypes from 'prop-types'

function Button(props) {
  const { label, className } = props
  return (<button { ...props } className={className} >{ label }</button>)
}
Button.displayName = 'Button'
Button.propTypes = {
  className: PropTypes.string,
  label: PropTypes.label
}
export default Button