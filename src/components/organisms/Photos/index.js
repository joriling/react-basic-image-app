import React, { useEffect } from 'react'
import { getPicsumPhotos } from '../../../services/photos'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from '../../atoms'
import styles  from '../../../styles/main.scss'
import { addImage, removeImage } from '../../../store/picsum-photos/actionCreators'

const Photos = () => {
  const dispatch = useDispatch()
  const picsumPhotosState = useSelector((state) => state.picsumPhotos?.items)
  const photos = useSelector((state) => state.picsumPhotos.photos)

  useEffect(() => {
    dispatch(getPicsumPhotos())
  }, [])

  const addImageClick = () => {
    const randomNumber = Math.floor(Math.random() * picsumPhotosState.length)
    const selectRandomImage = picsumPhotosState[randomNumber]
    dispatch(addImage(selectRandomImage))
  }

  const removeImageClick = () => {
    const removeRandomImage = photos[Math.floor(Math.random() * photos.length)]
    const newPhotos = photos.filter((item) => item.id !== removeRandomImage.id)
    dispatch(removeImage(newPhotos))
  }
  
  return (
    <div>
      <h1 className={styles.title}>Random Image Generator</h1>
      <div className={styles.buttonContainer}>
        <Button label="Add Random Image" onClick={addImageClick} className={styles.add} />
        <Button label="Remove Random Image" onClick={removeImageClick} className={styles.remove} />
      </div>
      <div className={styles.container}>
          {
            photos.map((item) => {
              return <img src={`https://picsum.photos/id/${item.id}/500/500`} alt={item.author} />
            })
          }
      </div>
    </div>)
}

export default Photos
