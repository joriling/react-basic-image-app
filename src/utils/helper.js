/**
 * Parse axios response to extract the server response
 *
 * @param {any} axiosPromise
 * @returns {Promise}
 */
 export function parseAxiosResponse(axiosPromise) {
  return axiosPromise.then(
    (res) => res.data,
    (err) => {
      if (err.networkError) return Promise.reject(err)
      return Promise.reject(err?.response?.data)
    }
  )
}