import produce from "immer";
import { getPicsumPhotos } from "../../services/photos";
import * as ActionTypes from './actionTypes'

const initialState = {
  items: [],
  state: 'pending',
  photos: [],
}

export default produce((draft, { type, payload }) => {
  switch (type) {
    case getPicsumPhotos.pending:
      draft.state = 'pending'
    break
    case getPicsumPhotos.fulfilled:
      draft.state = 'fullfilled'
      draft.items = payload
    break
    case getPicsumPhotos.failure:
      draft.state = 'failure'
    break
    case ActionTypes.ADD_IMAGE:
      draft.photos = [ ...draft.photos, payload ]
    break
    case ActionTypes.REMOVE_IMAGE:
      draft.photos = payload
    break
    default:
      return draft
  }
}, initialState)