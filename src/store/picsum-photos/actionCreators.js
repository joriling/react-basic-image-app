import * as types from './actionTypes'

export const addImage = (data) => {
  return {
    type: types.ADD_IMAGE,
    payload: { ...data },
  }
}

export const removeImage = (data) => {
  return {
    type: types.REMOVE_IMAGE,
    payload: data,
  }
}
