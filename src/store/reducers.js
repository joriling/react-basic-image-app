import { combineReducers } from 'redux'
import picsumPhotos from './picsum-photos/reducer'

const reducers = combineReducers({
  picsumPhotos
})

export default reducers